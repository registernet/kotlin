package link.anycar.study.kotlin.oop

open class A(open var x: Int) {
}

fun main(args: Array<String>) {
    var objectExpression = object:A(10) {
        override var x = 15
    }

    val obj = object {
        val x = 10
        val y = 15
    }

    println(objectExpression.x)
    println("Values in obj -> (${obj.x}, ${obj.y})")

    // object declaration
    Enroll.all.add(Person())
    Enroll.show()
}

class Person(var name: String = "小明")
object Enroll {
    val all = arrayListOf<Person>()
    fun show() = all.forEach { println(it)}
}