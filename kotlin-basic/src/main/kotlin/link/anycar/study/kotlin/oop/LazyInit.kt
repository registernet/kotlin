package link.anycar.study.kotlin.oop

fun main(args: Array<String>) {
    println(lazyValue)
    println(lazyValue)
}

val lazyValue: String by lazy(LazyThreadSafetyMode.NONE) {
    println("computing")
    "Hello world"
}