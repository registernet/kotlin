package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val arrayOfInt = intArrayOf(1, 2, 3, 4, 5)
    val arrayOfChar = charArrayOf('H', 'e', 'l', 'l', 'o')
    val arrayOfString = arrayOf("I", "love", "kotlin")

    println(arrayOfInt[2])
    println(arrayOfChar)
    for (str in arrayOfString) {
        println(str)
    }
}