package link.anycar.study.kotlin.basic

import java.lang.Exception

fun main(args: Array<String>) {
    var address: String? = null
    // null safe operator ?.
    println(address?.length)

    // if else
    val length = if (address != null) address.length else -1

    // elvis operator ?:
    val length2 = address?.length?:-1

    // force calling no matter null or not opeartor !!
    try {
        address!!.length
    } catch (ex: Exception) {
        ex.printStackTrace()
    }

    // type conversion as?
    var plateNumber: Int? = address as? Int
    println(plateNumber)


}