package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val uname = "Kotlin"
    val pwd = "password"
    val status: Boolean? = true
    var db = arrayOf("Kotlin", "password", 1)

    if (uname != db[0]) {
        println("用户名不存在")
    }
    if (pwd != db[1]) {
        println("密码错误")
    }

    if (uname == db[0] && pwd == db[1]) {
        if (db.get(2) == -1) {
            println("该用户已经删除")
        } else if (db.get(2) == 0) {
            println("该用户被禁用")
        } else {
            var str = if (status?: false) 1 else 0
            if (str == 1) {
                println("登陆成功，下次进入自动登陆状态")
            } else {
                println("登陆成功，但下次需要重新输入账号和密码")
            }
        }
    }
}
