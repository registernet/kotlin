package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val a = true
    val b = false

    println("逻辑或(||或者or) -> ${a or b}")
    println("逻辑与(&&或者and) -> ${a and b}")
    println("逻辑异或(xor) -> ${a xor b}")
    println("逻辑非(!) -> ${!a}")

}