package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val course = Course("语文", "100031")
}

class Course(var name: String, var code: String) {
    init {
        println("你选择的课程是${name}，编号是${code}")
    }
}