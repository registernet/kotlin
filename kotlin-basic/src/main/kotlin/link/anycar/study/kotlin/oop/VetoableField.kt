package link.anycar.study.kotlin.oop

import kotlin.properties.Delegates

fun main(args: Array<String>) {
    val vfUser = VFUser()
    println(vfUser.name)
    vfUser.name = "hello"
    println(vfUser.name)
    vfUser.name = "world"
    println(vfUser.name)
}

class VFUser {
    var name: String by Delegates.vetoable("<noname>") {
            prop, old, new ->
        println("Vetoing $prop: $old -> $new accept? ${new != "hello"}")
        new != "hello"
    }
}