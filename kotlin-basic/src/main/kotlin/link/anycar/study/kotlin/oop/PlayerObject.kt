package link.anycar.study.kotlin.oop

fun main(args: Array<String>) {
    var player = PlayerObject("Kotlin", 60.5, 1.59)
    println("您好${player.name}，您的身高是${player.height}米，体重是${player.weight}公斤，BIM值为${player.BIM}。")
    player.playBasketBall()
}

class PlayerObject(
        var name:String = "Kotlin",
        var weight: Double = 0.0,
        var height: Double = 0.0
        ) {
    val BIM: String
    get() {
        var b = this.weight/Math.pow(this.height, 2.0)
        if (b <= 18.5) {
            return "偏瘦"
        } else if (b in 18.5..23.9) {
            return "正常"
        } else if (b in 23.9..27.9) {
            return "过重"
        } else {
            return "肥胖"
        }
    }

    init {
        println("Constructor of PlayerObject called")
    }

    constructor(name: String, weight: Double, height: Double, int: Int): this(name, weight, height) {
        println("haha")
    }

    fun playBasketBall() {
        println("${name}正在打篮球。")
    }
}