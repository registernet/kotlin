package link.anycar.study.kotlin.collection

fun main(args: Array<String>) {
    println(listOf(Pair(1, 7), 2 to 8))

    val list = listOf(1, 2)
    println(list.zip(listOf(7, 8)))

    val list2 = listOf(2 to 1, 4 to 3, 6 to 5)
    println(list2)

    val list3 = listOf(1, 2, 3, 4, 5, 6)
    println(list3.partition { it % 2 == 0 })

    val list4 = list + list2 + list3
    println(list4)

    val unzipped = list2.unzip()
    val list5 = (unzipped.first + unzipped.second).sorted()
    println(list5)


}