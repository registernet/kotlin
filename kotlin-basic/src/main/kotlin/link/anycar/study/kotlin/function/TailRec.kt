package link.anycar.study.kotlin.function

fun main(args: Array<String>) {
    println(fac(5))
}

tailrec fun fac(n: Int): Double {
    var t: Double = 1.0
    if (n==0 || n==1) {
        t = 1.0
    } else {
        t = n * fac(n - 1)
    }

    return t
}