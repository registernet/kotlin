package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val a = 100
    println("左移 shl -> ${a.shl(2)}, 右移 shr -> ${a.shr(2)}, 无符号右移 ushr -> ${a.ushr(2)}")
    println("位与 and -> ${a.and(10)}, 位或 or -> ${a.or(10)}, 位异或 xor -> ${a.xor(10)}, 位非 1inv -> ${a.inv()}")
}