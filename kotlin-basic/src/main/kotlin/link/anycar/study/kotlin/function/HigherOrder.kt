package link.anycar.study.kotlin.function

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

fun main(args: Array<String>) {
    val lock = ReentrantLock()
    val value = lock(lock, {"Hello world"})
    println(value)

    println(lock(lock, ::pleaseSynchronize))
}

fun <T> lock(lock: Lock, body: () -> T) : T {
    lock.lock()
    try {
        return body()
    } finally {
        lock.unlock()
    }
}

fun pleaseSynchronize() = "Some Shared Resource"