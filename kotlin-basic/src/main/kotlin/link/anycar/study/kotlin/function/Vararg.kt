package link.anycar.study.kotlin.function

fun main(args: Array<String>) {
    val list = asList(1, 2, 3, 4, 5)
    println(list)
}

fun <T> asList(vararg items: T): List<T> {
    return items.asList()
}

