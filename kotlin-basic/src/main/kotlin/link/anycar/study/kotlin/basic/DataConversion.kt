package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val a = 10
    if (a is Int) {
        println("Int -> $a")
    }
    if (a !is Int) {
        println("Not Int -> $a")
    }

    val b:String? = a as? String
    println("b is -> $b")

    val c = a.toDouble()
    println(c)

    var str = readLine()
    println(str == "hello")
}