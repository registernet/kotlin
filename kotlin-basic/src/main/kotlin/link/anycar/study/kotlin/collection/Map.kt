package link.anycar.study.kotlin.collection

fun main(args: Array<String>) {
    val empty = emptyMap<Int, String>()
    val map = mapOf(1 to "one", 2 to "two", 3 to "three")
    val mMap = mutableMapOf(1 to "one", 2 to "two", 3 to "three")
    mMap.put(4, "four")
    mMap.put(5, "five")

    val hashMap = hashMapOf(1 to "one")
    hashMap.put(2, "two")
    hashMap.put(3, "three")

    val sortedMap = sortedMapOf(1 to "one", 2 to "two", 3 to "three")

    println(empty)
    println(map)
    println(mMap)
    println(hashMap)
    println(sortedMap)

    println()
    println(empty.size)
    println(hashMap.size)

    println()
    mMap.forEach { key, value ->
        println("key -> ${key}")
        println("value -> ${value}")
    }

    println()
    println(mMap.any { entry -> entry.value == "two" })
    println(mMap.all { entry -> entry.key > 0 })

    println()
    println(mMap.filter { entry -> entry.key > 2 })

}