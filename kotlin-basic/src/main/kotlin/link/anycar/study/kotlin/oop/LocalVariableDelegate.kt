package link.anycar.study.kotlin.oop

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun main(args: Array<String>) {
    ldelegate { "hello" }
    ldelegate { "world" }
}

fun ldelegate(computeString:() -> String) {
    val savedString by lazy(computeString)
    println(savedString)
    if (savedString != "hello" && computeString() != "hello") {
        println("saved")
    }
}