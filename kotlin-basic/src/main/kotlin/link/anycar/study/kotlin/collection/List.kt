package link.anycar.study.kotlin.collection

fun main(args: Array<String>) {
    val devices1 = listOf("显示器", "键盘", "鼠标", "主机")
    val devices2 = listOf("联想笔记本", "戴尔笔记本", "外星人笔记本")
    val devices = listOf(devices1, devices2)

    println("------------------------list--------------------")
    println("Index: ${devices.indexOf(devices1)}的value内容：${devices.get(0)}")
    println()

    devices.forEach(::println)
    println()

    for (device in devices) {
        println(device)
    }
    println()

    println("--------------------mutable list--------------------")
    val mDevices = mutableListOf<String>()
    mDevices.addAll(devices1)
    mDevices.add("电源")
    println(mDevices)
    println(mDevices::class.java)

}