package link.anycar.study.kotlin.oop

import kotlin.properties.Delegates

fun main(args: Array<String>) {
    val ofUser = OFUser()
    println(ofUser.name)
    ofUser.name = "hello world"
    ofUser.name = "hello universe"
}

class OFUser {
    var name: String by Delegates.observable("<noname>") {
        prop, old, new ->
            println("$prop: $old -> $new")
    }
}