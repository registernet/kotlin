package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    println("Hello world, here is a kotin project.")

    var fooBar = FooBar()
    fooBar.doBar()
    println(fooBar.doFoo())


    /*
     * lambda expression
     */
    val list = listOf(1, 3, 5, 7, 9, 11, 13, 15, 17, 19)
    val sum = list.filter { it > 10 }.map { it * 2 }.sum()
    val list2 = ArrayList<Int>()
    list.filter { it <= 10 }.map { it * it }.toCollection(list2)
    println("sum is ${sum}")
    println("list2 is ${list2}")

    val bool = true
    val bool2: Boolean = false
    println("Boolean value bool -> ${bool}, bool2 -> ${bool2}")

    val hello = "${"$$"}"
    println(hello)

}


/**
 * interface and interface extension
 */

interface Bar {
    fun doBar()
}

interface Foo<out T : Any> : Bar {
    fun doFoo() : T
}

/**
 * interface implementation
 */

class FooBar() : Foo<String> {
    override fun doBar() {
        println("Great, you do bar!")
    }

    override fun doFoo(): String {
        return "Value from doFoo()"
    }

}


