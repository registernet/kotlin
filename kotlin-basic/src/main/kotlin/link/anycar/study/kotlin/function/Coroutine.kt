package link.anycar.study.kotlin.function

import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking {
    async(::susSayHello)

    println("Hello ")

    coroutineScope {
        val job = launch {
            println("value from inner launch")
        }
        delay(1000)
        println("new Task")
    }

    runBlocking { delay(2000) }
}



suspend fun susSayHello() {
    delay(1000)
    println("world")
}

fun <T> async(block: suspend () -> T) {
    GlobalScope.launch { block() }
}