package link.anycar.study.kotlin.oop

fun main(args: Array<String>) {
    val mfUser = MFUser(mapOf("name" to "xiaoming", "age" to 10))
    println(mfUser.name)
    println(mfUser.age)
    println()

    val mmfUser = MMFUser(mutableMapOf())
    mmfUser.name = "小明"
    mmfUser.age = 10
    println(mmfUser.name)
    println(mmfUser.age)
}

class MFUser(map: Map<String, Any?>) {
    val name: String by map
    val age: Int by map
}

class MMFUser(map: MutableMap<String, Any?>) {
    var name: String by map
    var age: Int by map
}