package link.anycar.study.kotlin.oop

fun main(args: Array<String>) {
    val user = User("1", "小明", 15, "广州天河")
    val article = Article("1", "1", "计算机学习", "关于计算机学习的文章", "本文介绍了计算机相关的所有技术")

    println(user)
    println(article)

}

data class User(
        var id: String,
        var name: String,
        var age: Int,
        var address: String)

data class Article(
        var id: String,
        var userId: String,
        var title: String,
        var subTitle: String,
        var content: String)