package link.anycar.study.kotlin.oop

import kotlin.reflect.KProperty

fun main(args: Array<String>) {
    val hello = Hello()
    hello.value = "12345"
    println(hello.value)
}

class Hello {
    var value: String by Delegate()
}

class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thanks for delegating."
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value saved to ${property.name} in $thisRef.")
    }
}