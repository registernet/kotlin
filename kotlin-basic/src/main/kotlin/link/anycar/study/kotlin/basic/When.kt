package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    val auto: Boolean? = true
    val status = 1

    when (status) {
        -1 -> println("该用户已经删除")
        0 -> println("该用户被禁用")
        in 0..10 -> {
            var autoMode = if (auto?: false) 1 else 0
            if (autoMode == 1) {
                println("登陆成功，下次将自动登陆")
            } else {
                println("登陆成功，但下次需要重新输入账号密码")
            }
        }
        else -> {
            println("系统忙，请稍后再试")
        }
    }
}