package link.anycar.study.kotlin.basic

fun main(args: Array<String>) {
    whileLoop()
    println("\n")
    forLoop()
}

fun whileLoop() {
    println("------------------while-------------------")
    var start = 1
    var sum = 0
    val target = 1024
    while(sum < target) {
        sum += start++
    }
    println("Sum up start from 1 to ${start - 1} exceeds ${target}, the summary is ${sum}")

    start = 1
    sum = 0
    do {
        sum += start++
    } while (sum < target)
    println("Sum up start from 1 to ${start - 1} exceeds ${target}, the summary is ${sum}")
}

fun forLoop() {
    println("------------------ for -------------------")

    val arr = arrayOf("I", "LOVE", "KOTLIN")
    for(item in arr) {
        println(item)
    }
    println()

    for (someInt in 1..10 step 2) {
        println("I'll say this 5 times -> ${someInt}")
    }
    println()

    for (index in arr.indices step 1) {
         println(index)
    }
}