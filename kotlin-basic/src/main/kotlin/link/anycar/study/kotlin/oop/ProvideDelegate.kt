package link.anycar.study.kotlin.oop

import kotlin.reflect.KProperty

fun main(args: Array<String>) {
    val bound = Bound()
    bound.id = "helloworld"
    println(bound.name)
    println(bound.id)
    println(bound.id)
}

class Bound {
    var name by ResourceLoader("name")
    var id by ResourceLoader("id")
}

class ResourceLoader(id: Any) {
    operator fun provideDelegate(thisRef: Any, prop: KProperty<*>) : PDDelegate {
        checkProperty(thisRef, prop.name)
        return PDDelegate()
    }

    private fun checkProperty(thisRef: Any, name: String) {
        println("Checking property ${thisRef}.${name}")
    }
}

class PDDelegate{
    var value = "<novalue>"
    operator fun getValue(thisRef: Any, prop: KProperty<*>): String {
        println("getting value")
        return value
    }

    operator fun setValue(thisRef: Any, prop: KProperty<*>, value: String) {
        println("setting value")
        this.value = value
    }
}