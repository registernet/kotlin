package link.anycar.study.kotlin.collection

fun main(args: Array<String>) {
    val empty = emptySet<String>()
    val set = setOf(1, 2, 3, 4, 5)
    val mset = mutableSetOf(1, 2, 3, 4 ,5)
    val hashSet = hashSetOf(1, 2, 3, 4, 5)
    val linkedSet = linkedSetOf(1, 2, 3, 4, 5)
    val sortedSet = sortedSetOf(1, 2, 3, 4, 5)

    println(empty)
    println(set)
    mset.add(6)
    println(mset)
    println(hashSet)
    println(linkedSet)
    println(sortedSet)
}