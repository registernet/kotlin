package link.anycar.study.kotlin.function

import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock

fun main(args: Array<String>) {
    val lock = ReentrantLock()
    val value = inlineLock(lock, {"Hello world"})
    println(value)

    println(inlineLock(lock, ::pleaseSynchronize))
}

inline fun <T> inlineLock(lock: Lock, body: () -> T) : T {
    lock.lock()
    try {
        return body()
    } finally {
        lock.unlock()
    }
}

inline fun inlinePleaseSynchronize() =  "Some Shared Resource"