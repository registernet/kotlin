package link.anycar.study.kotlin.function

fun main(args: Array<String>) {
    sayHello()
    sayHello("小红")

    println(genCity(city = "广州市", street = "广和路"))
}

fun sayHello(name: String = "小明") {
    println("${name}您好！")
}

fun genCity(street: String, district: String = "天河区", city: String): String {
    return "${city}${district}${street}"
}