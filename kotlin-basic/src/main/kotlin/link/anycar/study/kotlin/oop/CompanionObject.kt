package link.anycar.study.kotlin.oop

class CompanionObject {
    companion object Factory { // the name Factory can be ignored
        fun create() = CompanionObject()
    }
}

fun main(args: Array<String>) {
    val co = CompanionObject.create()
    println(co)
}